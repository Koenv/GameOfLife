/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author Gebruiker
 */
public class Land extends Gridsection implements Serializable{

    /**
     * Constructor
     * @param x
     * @param y
     * @param size 
     */
    public Land(int x, int y, int size) {
        super(x, y, size);
        color = Color.WHITE;
    }

}
