/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author Koen
 */
public class Plant extends Organism implements Serializable {

    private boolean comesToLive;
    private int timesEaten;
    private int timeDeath;
    private int size;

    /**
     * Default constructor
     */
    
    

    /**
     * constructor
     * @param x
     * @param y
     * @param size 
     */
    public Plant(int x, int y, int size) {
        super(x, y);
        this.size = size;
        color = Color.green;
        energy = size;
    }
    
    
//    /**
//     *
//     * @return 
//     */
//    public boolean isComesToLive() {
//        if (timeDeath < 100) {
//            timeDeath = timeDeath++;
//            return false;
//        } else {
//            return comesToLive = true;
//        }
//    }
//
//    /**
//     * gets the times plants bin eaten
//     * @return 
//     */
//    public int getTimesEaten() {
//        return timesEaten;
//    }
//
//    /**
//     *
//     * @param eat 
//     */
//    public void getsEaten(int eat) {
//        if (timesEaten >= 1) {
//            timesEaten = timesEaten - 10;
//        }
//    }
//
//    /**
//     * Set the growt
//     * @param grows 
//     */
//    public void grows(int grows) {
//        timesEaten = timesEaten + 10;
//    }

    public int getSize() {
        return size;
    }
}
