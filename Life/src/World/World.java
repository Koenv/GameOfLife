/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Koen
 */
public class World implements ISimulationWorld, Serializable {

    private int height;
    private int width;
    private List<Gridsection> gridSections = new ArrayList<>();
    private List<Organism> organisms = new ArrayList<>();
    private List<Organism> intercourseOrganisms = new ArrayList<>();

    public World() {
    }

    public void saveFile() {

        try {
            FileOutputStream fos = new FileOutputStream("C:\\Users\\Koen\\Documents\\NetBeansProjects\\SWELife\\SWECasusLife\\state.tmp");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
//        oos.writeObject(getGridsections());
            oos.writeObject(organisms);
            oos.close();
            fos.close();
            System.out.println("saved");
            System.out.println(gridSections.size());
            saveFile2();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void saveFile2() {

        try {
            FileOutputStream fos = new FileOutputStream("C:\\Users\\Koen\\Documents\\NetBeansProjects\\SWELife\\SWECasusLife\\state2.tmp");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(gridSections);
//            oos.writeObject(organisms);
            oos.close();
            fos.close();
            System.out.println("saved");
            System.out.println(gridSections.size());
//            saveFile2();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void loadFile() {
        try {

            FileInputStream fis = new FileInputStream("C:\\Users\\Koen\\Documents\\NetBeansProjects\\SWELife\\SWECasusLife\\state.tmp");
            ObjectInputStream ois = new ObjectInputStream(fis);
            organisms = (ArrayList<Organism>) ois.readObject();
//       gridSections = (ArrayList<Gridsection>) ois.readObject();
            ois.close();
            fis.close();
            loadFile2();
            System.out.println("load");
            System.out.println(getOrganisms().size());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void loadFile2() {
        try {
            FileInputStream fis = new FileInputStream("C:\\Users\\Koen\\Documents\\NetBeansProjects\\SWELife\\SWECasusLife\\state2.tmp");
            ObjectInputStream ois = new ObjectInputStream(fis);
//       organisms = (ArrayList<Organism>) ois.readObject();
            gridSections = (ArrayList<Gridsection>) ois.readObject();
            System.out.println(getGridsections().size());
            ois.close();
            fis.close();
            System.out.println("load2");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructor
     *
     * @param height
     * @param width
     */
    public World(int height, int width) {
        this.height = height;
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    /**
     * Gets the list of gridsections
     *
     * @return
     */
    public List<Gridsection> getGridsections() {
        return gridSections;
    }

    /**
     * Gets de list of organisms
     *
     * @return
     */
    public List<Organism> getOrganisms() {
        return organisms;
    }
    
    public List<Organism> getIntercourseOrganisms(){
        return intercourseOrganisms;
    }

    /**
     * Adds the animals to the world
     *
     * @param aantHerb
     * @param aantCarni
     * @param aantNon
     * @param aantOmni
     */
    public void addAnimals(int aantHerb, int aantCarni, int aantNon, int aantOmni) {
        int herb = 1;
        int carni = 1;
        int non = 1;
        int omni = 1;
        int minEnergy = 0;
        int maxEnergy = 200;
        while (herb <= aantHerb) {
            int rdmpoint1 = new Random().nextInt(40) * 10 + 60;
            int rdmpoint2 = new Random().nextInt(80) * 10 + 60;
            int rdmPointx = new Random().nextInt(40) * 10 + 500;
            int rdmPointy = new Random().nextInt(80) * 10 + 60;
            int rdmStamina = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs = new Random().nextInt(8) + 1;
            int rdmStamina2 = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs2 = new Random().nextInt(8) + 1;
            Digestion digestion = Digestion.Plant;
            Animal nieuwAnimal1 = new Animal(rdmStamina2, rdmPointx, rdmPointy, digestion, rdmLegs2);
            Animal nieuwAnimal8 = new Animal(rdmStamina, rdmpoint1, rdmpoint2, digestion, rdmLegs);
            organisms.add(nieuwAnimal1);
            organisms.add(nieuwAnimal8);
            herb++;
        }

        while (carni <= aantCarni) {
            int rdmpoint1 = new Random().nextInt(40) * 10 + 60;
            int rdmpoint2 = new Random().nextInt(80) * 10 + 60;
            int rdmPointx = new Random().nextInt(40) * 10 + 500;
            int rdmPointy = new Random().nextInt(80) * 10 + 60;
            int rdmStamina = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs = new Random().nextInt(8) + 1;
            int rdmStamina2 = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs2 = new Random().nextInt(8) + 1;
            Digestion digestion = Digestion.Meat;
            Animal nieuwAnimal2 = new Animal(rdmStamina2, rdmPointx, rdmPointy, digestion, rdmLegs2);
            Animal nieuwAnimal7 = new Animal(rdmStamina, rdmpoint1, rdmpoint2, digestion, rdmLegs);
            organisms.add(nieuwAnimal2);
            organisms.add(nieuwAnimal7);
            carni++;
        }

        while (omni <= aantOmni) {
            int rdmpoint1 = new Random().nextInt(40) * 10 + 60;
            int rdmpoint2 = new Random().nextInt(80) * 10 + 60;
            int rdmPointx = new Random().nextInt(40) * 10 + 500;
            int rdmPointy = new Random().nextInt(80) * 10 + 60;
            int rdmStamina = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs = new Random().nextInt(8) + 1;
            int rdmStamina2 = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs2 = new Random().nextInt(8) + 1;
            Digestion digestion = Digestion.Both;
            Animal nieuwAnimal3 = new Animal(rdmStamina2, rdmPointx, rdmPointy, digestion, rdmLegs2);
            Animal nieuwAnimal6 = new Animal(rdmStamina, rdmpoint1, rdmpoint2, digestion, rdmLegs);
            organisms.add(nieuwAnimal3);
            organisms.add(nieuwAnimal6);
            omni++;
        }

        while (non <= aantNon) {
            int rdmpoint1 = new Random().nextInt(40) * 10 + 60;
            int rdmpoint2 = new Random().nextInt(80) * 10 + 60;
            int rdmPointx = new Random().nextInt(40) * 10 + 500;
            int rdmPointy = new Random().nextInt(80) * 10 + 60;
            int rdmStamina = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs = new Random().nextInt(8) + 1;
            int rdmStamina2 = new Random().nextInt(maxEnergy - minEnergy) + 100;
            int rdmLegs2 = new Random().nextInt(8) + 1;
            Digestion digestion = Digestion.Nothing;
            Animal nieuwAnimal4 = new Animal(rdmStamina, rdmpoint1, rdmpoint2, digestion, rdmLegs);
            Animal nieuwAnimal5 = new Animal(rdmStamina2, rdmPointx, rdmPointy, digestion, rdmLegs2);
            organisms.add(nieuwAnimal4);
            organisms.add(nieuwAnimal5);
            non++;
        }
    }

    /**
     * Adds the water to the world.
     */
    public void addWater() {
        int x = 0;
        int y = 0;
        int xM = 0;
        int yM = 0;
        int xR = 900;
        int yR = 0;
        int size = 10;

        // adds the water vertically.
        for (int i = 0; i < 93; i++) {
            for (int j = 0; j < 6; j++) {
                Gridsection water1 = new Water(x, y, size);
                Gridsection water2 = new Water(xR, yR, size);
                gridSections.add(water1);
                gridSections.add(water2);
                x = x + 10;
                xR = xR + 10;
            }
            xR = 900;
            x = 0;
            for (int k = 0; k < 2; k++) {
                for (int l = 0; l < 4; l++) {
                    Gridsection waterM = new Water(xM, yM, size);
                    gridSections.add(waterM);
                    xM = xM + 10;
                }
                xM = 460;
            }
            yM = yM + 10;
            y = y + 10;
            yR = yR + 10;
        }

        int xH = 60;
        int xL = 60;
        int yL = 870;
        int yH = 0;

        // adds the water horizontally.
        for (int h = 0; h < 6; h++) {
            for (int g = 0; g < 84; g++) {
                Gridsection waterH = new Water(xH, yH, size);
                Gridsection waterL = new Water(xL, yL, size);
                gridSections.add(waterL);
                gridSections.add(waterH);
                xL = xL + 10;
                xH = xH + 10;
            }
            xH = 60;
            xL = 60;
            yL = yL + 10;
            yH = yH + 10;
        }
    }

    /**
     * Adds the land to the world
     */
    public void addLand() {
        int x = 60;
        int y = 60;
        int xP = 500;
        int yP = 60;
        int size = 10;

        for (int i = 0; i <= 80; i++) {
            for (int j = 0; j <= 39; j++) {
                Gridsection land1 = new Land(x, y, size);
                Gridsection land2 = new Land(xP, yP, size);
                gridSections.add(land1);
                gridSections.add(land2);
                x = x + 10;
                xP = xP + 10;
            }
            xP = 500;
            x = 60;
            Land land1 = new Land(x, y, size);
            Land land2 = new Land(xP, yP, size);
            gridSections.add(land1);
            gridSections.add(land2);
            y = y + 10;
            yP = yP + 10;
        }

//        System.out.println("het aantal grids voor landen = " + gridSections.size());
    }

    /**
     * Adds the obstacles to the world
     *
     * @param aantalObstacles
     */
    public void addObstacle(int aantalObstacles) {
        int i = 0;
        while (i <= aantalObstacles) {
            //random x and y obstacles for left livingarea
            int rdmpoint1 = new Random().nextInt(40) * 10 + 60;
            int rdmpoint2 = new Random().nextInt(81) * 10 + 60;
            //random x and y obstacles for right livingarea
            int rdmPointx = new Random().nextInt(40) * 10 + 500;
            int rdmPointy = new Random().nextInt(81) * 10 + 60;
            //make object obstacle
            Obstacle obstacle1 = new Obstacle(rdmpoint1, rdmpoint2, 10);
            Obstacle obstacle2 = new Obstacle(rdmPointx, rdmPointy, 10);
            //fil list with obstacle's
            gridSections.add(obstacle1);
            gridSections.add(obstacle2);
            i++;
        }
    }

    /**
     * Adds the plants to the world
     *
     * @param aantPlants
     */
    public void addPlants(int aantPlants) {
        int i = 0;
        while (i <= aantPlants) {

            int rdmpoint1 = new Random().nextInt(40) * 10 + 60;
            int rdmpoint2 = new Random().nextInt(81) * 10 + 60;
            int rdm = new Random().nextInt(100) + 1;
            Plant nieuwPlant1 = new Plant(rdmpoint1, rdmpoint2, rdm);

            int rdmPointx = new Random().nextInt(40) * 10 + 500;
            int rdmPointy = new Random().nextInt(81) * 10 + 60;
            Plant nieuwPlant2 = new Plant(rdmPointx, rdmPointy, rdm);
            organisms.add(nieuwPlant1);
            organisms.add(nieuwPlant2);
            i++;
        }
    }

    @Override
    public void move(Organism o) {
        //sla oude positie op
        int xOud = o.getX();
        int yOud = o.getY();

        //set nieuwe positie
        o.setX(o.getX());
        o.setY(o.getY());

        //sla nieuwe locatie op
        int xNieuw = o.getX();
        int yNieuw = o.getY();

        for (Organism org : getOrganisms()) 
        {
            if (org instanceof Plant) 
            {
                org.setEnergy(org.getEnergy() + 1);
//                System.out.println(org.getEnergy());
                if (org.getX() == xNieuw && org.getY() == yNieuw) 
                {
                    Color colorgrid = org.getColor();
                    if (colorgrid == Color.green && o.getDigestion() == Digestion.Plant || o.getDigestion() == Digestion.Both) 
                    {
                        eatPlant(org);
                    }
                }
            }
             
                if (org.getX() == xNieuw && org.getY() == yNieuw
                        && organisms.indexOf(org) != organisms.indexOf(o))
                    
                {
                    boolean eat = false;
                    if(org.getHunger() < org.getEnergy() || o.getHunger() < o.getEnergy())
                    {
                    eatAnimal(org, o);
                    eat = true;
                    }
                
                    else if(org.getLibido() < org.getEnergy() && o.getLibido() < o.getEnergy() && eat == false);
                 {
                    Organism nieuwO = new Animal(o.getStamina(), o.getX(), o.getY(), o.getDigestion(), o.getLegs());
                    Organism nieuwOrg = new Animal(org.getStamina(), org.getX(), org.getY(), org.getDigestion(), org.getLegs());
                    intercourseOrganisms.add(nieuwO);
                    intercourseOrganisms.add(nieuwOrg);
//                    System.out.println("Size intercourse list= " + intercourseOrganisms.size());
                 }
                }
                      
                    //intercourse(org, o);
//                    System.out.println("Diernr " + organisms.indexOf(org));
                
            }
        

        for (Gridsection g : getGridsections()) {
            if (g.getX() == xNieuw && g.getY() == yNieuw) {
                Color colorgrid = g.getColor();
                if (colorgrid == Color.BLACK) {
                    o.setXback(xOud);
                    o.setYback(yOud);
                    o.setDirection(o.randomDirection());
                    o.setEnergy(o.getEnergy() / 2);
                } 
                
                if (colorgrid == Color.blue) {
                    if (o.getMinSwimmingEnergyLevel() < o.getEnergy() ) 
                    {
                        swim(o);
                    }
                    if(o.getMinSwimmingEnergyLevel() > o.getEnergy())
                    {
                    o.setXback(xOud);
                    o.setYback(yOud);
                    o.setDirection(o.randomDirection());
                    long procentEnergy = o.getEnergy() / o.getStamina() * 100;
//                    System.out.println("Not enough energy to swim, aantal procent: " + procentEnergy);  
                    }
                   
                   
                }
               
            }
        }
    }

    /**
     * makes the animal swim
     *
     * @param o
     */
    @Override
    public void swim(Organism o) {

        //System.out.println("energy before: " + o.getEnergy());
        int swimEnergyCost = o.getEnergy() - o.getLegs();
        o.setEnergy(swimEnergyCost);
        //System.out.println("Energy Costs: " + swimEnergyCost + " Legs: " + o.getLegs() + "  energy animal: " + o.getEnergy());
    }

    /**
     *
     * @param a
     * @param b
     */
    @Override
    public void eatAnimal(Organism a, Organism b) {
        int aStrength = a.getStrength();
        int bStrength = b.getStrength();
        int aStamina = a.getStamina();
        int bStamina = b.getStamina();
        //check which animal is stronger
        if (a.getStrength() >= b.getStrength()
                && a.getDigestion() == Digestion.Both
                || a.getDigestion() == Digestion.Meat) {
            int verschil = Math.abs(a.getStrength() - b.getStamina());
            a.setEnergy(a.getEnergy() + verschil);
            b.setEnergy(b.getEnergy() - verschil);
//            System.out.println("Animal B van gegeten energie over: " + b.getEnergy());
        }
        if (a.getStrength() < b.getStrength()
                && b.getDigestion() == Digestion.Both
                || b.getDigestion() == Digestion.Meat) {
            int verschil = Math.abs(b.getStrength() - a.getStamina());
            b.setEnergy(b.getEnergy() + verschil);
            a.setEnergy(a.getEnergy() - verschil);
//            System.out.println("Animal A van gegeten, energie over: " + a.getEnergy());
        }
        else
        {
//            System.out.println("Eten niet gelukt");
//        System.out.println("engery a = " + a.getEnergy()
//                + "\n energy b = " + b.getEnergy() + " Stamina= " + a.getStamina());
        }
    }

    /**
     *
     * @param animal1
     * @param animal2
     * @return nieuw animal wat combi is van de 2 animals.
     */
    @Override
    public void eatPlant(Organism plant) {
        int eet = plant.getEnergy() - 10;
        plant.setEnergy(eet);
//        System.out.println("Plant gegeten");
    }

    @Override
    public Organism intercourse(Organism o1) {

        for(Organism o2 : intercourseOrganisms){
               
        if (o1.getX() == o2.getX() && o1.getY() == o2.getY()
                        && intercourseOrganisms.indexOf(o1) != intercourseOrganisms.indexOf(o2))
        {
        int legsBaby = ((o1.getLegs() + o2.getLegs()) / 2);
//        System.out.println("aantal benen " + legsBaby);
        int staminaBaby = ((o1.getStamina() + o2.getStamina()) / 2);
//        System.out.println("stamina baby " + staminaBaby);
//        System.out.println("");
        int rand = new Random().nextInt(Digestion.values().length);
        Digestion digestionBaby = Digestion.values()[rand];

        if (o1.getDigestion() == Digestion.Meat && o2.getDigestion() == Digestion.Meat) {
            digestionBaby = Digestion.Meat;
//            System.out.println("meat");
        }
        if (o1.getDigestion() == Digestion.Plant && o2.getDigestion() == Digestion.Plant) {
            digestionBaby = Digestion.Plant;
//            System.out.println("Plant");
        }
        if (o1.getDigestion() == Digestion.Nothing && o2.getDigestion() == Digestion.Nothing) {
            digestionBaby = Digestion.Nothing;
//            System.out.println("Niks");
        }
        
        Organism nieuwAnimal = new Animal(staminaBaby, o1.getX() + 100, o1.getY() + 100, digestionBaby, legsBaby);
        organisms.add(nieuwAnimal);
        o1.setX(3);
        o1.setY(3);
        o2.setX(1);
        o2.setX(1);
//        System.out.println("nieuw animal");
        return nieuwAnimal;
        }
        }
        return null;
    }

    

    public void clearIntercourseList()
    {
        intercourseOrganisms.clear();
    }
}
