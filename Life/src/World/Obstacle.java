/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;

/**
 *
 * @author Koen
 */
public class Obstacle extends Gridsection implements Serializable {

    /**
     * constructor
     *
     * @param x
     * @param y
     * @param size
     */
    public Obstacle(int x, int y, int size) {
        super(x, y, size);
        color = Color.BLACK;
    }
}
