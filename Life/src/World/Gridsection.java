/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author Koen
 */
public class Gridsection implements Serializable {

    private int x;
    private int y;
    private int size = 10;
    protected Color color;

    /**
     * Constructor
     *
     * @param x
     * @param y
     * @param size
     */
    public Gridsection(int x, int y, int size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }

    /**
     * sets the default color black
     * @return 
     */
    public Color defaultColor() {
        return Color.BLACK;
    }

    /**
     * get x position
     *
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     * get y position
     *
     * @return
     */
    public int getY() {
        return y;
    }

    /**
     * get the color
     *
     * @return
     */
    public Color getColor() {
        return color;
    }

    /**
     * get the size
     *
     * @return
     */
    public int getSize() {
        return size;
    }
}
