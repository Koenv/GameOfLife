/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

//import java.awt.Color;
import java.awt.Color;
import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author Koen
 */
public class Organism implements Serializable {

    protected Color color;
    protected int energy;
    private boolean isDeath;
    private int x;
    private int y;
    private Direction direction;
    private int stamina;
    private int legs;
    private Digestion digestion;

    /**
     * Constructor
     *
     * @param x
     * @param y
     */
    public Organism(int x, int y) {
        this.x = x;
        this.y = y;
        isDeath = false;
    }

    public int getStrength() {
        return 0;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
        
    }

    public int getStamina() {
        return stamina;
    }

    public int getLegs() {
        return legs;
    }

    /**
     * get x position
     *
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     * get y position
     *
     * @return
     */
    public int getY() {
        return y;
    }

    public int getWeight() {
        return 0;
    }

    public int getMinSwimmingEnergyLevel() {
        return 0;
    }

    public void setX(int x) {
        if (getDirection() == Direction.EAST) {
            if (x == 950) {
                this.x = 0;
            } else {
                this.x = x + 10;
            }
        }
        if (getDirection() == Direction.WEST) {
            if (x == 0) {
                this.x = 950;
            } else {
                this.x = x - 10;
            }
        }
        if (getDirection() == Direction.NORTHEAST) {
            if (x == 950) {
                this.x = 0;
            } else {
                this.x = x + 10;
            }
        }
        if (getDirection() == Direction.NORTHWEST) {
            if (x == 0) {
                this.x = 950;
            } else {
                this.x = x - 10;
            }
        }
        if (getDirection() == Direction.SOUTHWEST) {
            if (x == 0) {
                this.x = 950;
            } else {
                this.x = x - 10;
            }
        }
        if (getDirection() == Direction.SOUTHEAST) {
            if (x == 950) {
                this.x = 0;
            } else {
                this.x = x + 10;
            }
        }

    }

    public void setY(int y) {
        if (getDirection() == Direction.NORTH) {
            if (y == 0) {
                this.y = 920;
            } else {
                this.y = y - 10;
            }
        }
        if (getDirection() == Direction.SOUTH) {
            if (y == 920) {
                this.y = 0;
            } else {
                this.y = y + 10;
            }
        }
        if (getDirection() == Direction.NORTHWEST) {
            if (y == 0) {
                this.y = 920;
            } else {
                this.y = y - 10;
            }
        }
        if (getDirection() == Direction.NORTHEAST) {
            if (y == 0) {
                this.y = 920;
            } else {
                this.y = y - 10;
            }
        }
        if (getDirection() == Direction.SOUTHEAST) {
            if (y == 920) {
                this.y = 0;
            } else {
                this.y = y + 10;
            }
        }
        if (getDirection() == Direction.SOUTHWEST) {
            if (y == 920) {
                this.y = 0;
            } else {
                this.y = y + 10;
            }
        }
    }

    /**
     * get color
     *
     * @return
     */
    public Color getColor() {
        return color;
    }

    public int getEnergy() {
        return energy;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getHunger(){
        return 0;
    }
    
    public int getLibido(){
        return 0;
    }
    /**
     *
     * @return boolean
     */
    public boolean isDeath() {
        if (energy < 1) {
            return isDeath = true;
        } else {
            return isDeath = false;
        }
    }

    /**
     *
     * @param isDeath
     */
    public void setIsDeath(boolean isDeath) {
        this.isDeath = isDeath;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * returns a random direction
     *
     * @return
     */
    public Direction randomDirection() {
        int rand = new Random().nextInt(Direction.values().length + 1);
        return Direction.values()[rand];
    }

    public void setXback(int x) {
        this.x = x;

    }

    public void setYback(int y) {
        this.y = y;

    }
    
    public Digestion getDigestion() {
        return digestion;
    }

}
