/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

/**
 *
 * @author Koen
 */
public interface ISimulationWorld {

    /**
     * Rik haal alle beesten op uit de lijst organisms creeer een lijst met
     * animals in deze methode zodat je alleen de animals hebt nu ga je kijken
     * welke direction de animal heeft zodat je weet welk vakje hij naar toe
     * moet de snelheid bepaalt hoeveel vakjes hij kan lopen. Dit moet voor stap
     * voor stap want hij kan een obstacle/beest/plant/water tegen komen Eerst
     * zet je 1 stap in de richting, dan ga je checken in de vakjes om je heen
     * of er iets staat. Een lijstje maken met alle gridsection om het beest
     * heen. 10 gridsections krijg je dan. Dan kun je checken of er iets in 1
     * van die vakjes staat(beest, water, obstacle, plant)
     *
     * staat er een beest of plant in 1 van die vakje ga je kijken of het animal
     * honger heeft. Zoja kiest hij random of hij de plant of het beest eet(als
     * hij omnivoor is) hij loopt hierna niet meer verder, maar gaat de methode
     * eat aanroepen. aantal stappen wordt dan naar 0 gezet
     *
     * als het vakje waar hij op staat een obstacle is dan gaat er half van zijn
     * energie af en wordt de direction opnieuw geset. aantal stappen naar 0;
     *
     * als het vakje waar hij op staat water is, kijkt hij of ie kan zwemmen, zo
     * niet gaat hij een vakje terug, en krijgt hij een andere
     *
     *
     *
     *
     */
    public void move(Organism o);

    /**
     * Rene Eat check welk beest of plant aan een aansluitend gridvakje staat.
     * Als het een plant is, kan de beesten ominvoor en herbivoor de plant eten
     * en neemt 10% van energie weg. Tenzij het betreffende beest al genoeg
     * energie heeft. Als het een beest is, word eerst gekeken of er genoeg
     * energie is (het beest geen honger heeft) Anders gaat een van de twee
     * beesten eten. Als beide beesten honger hebben gaat het beest met de
     * hoogste strengt het ander beest eten. De hoeveelheid opgegeten energie
     * wordt bepaald door het verschil van de strength van de jager en de
     * stamina van de gegetene. Het gegeten beest sterft als er geen energie
     * over is
     *
     * @param a
     * @param b
     */
    public void eatAnimal(Organism a, Organism b);

    public void eatPlant(Organism plant);

    /**
     * Koen let two animals have intercourse. Wanneer een beest in de buurt
     * staat wordt er gekeken naar de hitsigheid van het beest(move checkt dit)
     * Als de hitsigheid van beide Animals hoog genoeg wordt er gepaard, tijdens
     * het paren wordt niet gegeten. Uit dit paren komt een nieuw kind (Animal)
     * met een hoeveelheid startenergie, welke bepaald wordt door de erfelijke
     * eigenschappen. Elke erfelijke eigenschap bij geboorte is een gemiddelde
     * waarde van de eigenschappen van hun ouders plus of min een random waarde,
     * die maximaal 10% van het maximale verschil van de eigenschappen van de
     * ouders is. De positie van het nieuwe kind wordt gezet op de locatie van
     * de ouder die de hoogste energiewaarde heeft.
     *
     * @param currentAnimal
     * @param otherAnimal
     */
    public Organism intercourse(Organism currentOrganism);

    /**
     * Koen let a animal swim in the water. Een beest heeft een zwemdrempel: dit
     * is 50% van stamina waaronder beesten willen zwemmen. Oversteken van water
     * kost per beurt evenveel energie units als er poten aan het beest zitten.
     * Tijdens het zwemmen kan er niet gegeten of gepaard worden.
     */
    public void swim(Organism organism);

}
