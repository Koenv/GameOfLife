/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.io.Serializable;

/**
 *
 * @author Koen
 */
public class Water extends Gridsection implements Serializable {

    /**
     * Constructor
     * @param x
     * @param y
     * @param size 
     */
    public Water(int x, int y, int size) {
        super(x, y, size);
        color = Color.blue;
    }
}
