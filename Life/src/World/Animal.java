/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

import java.awt.Color;
import java.awt.Point;
import java.io.Serializable;
import java.util.Random;

/**
 *
 * @author Koen
 */
public final class Animal extends Organism implements Serializable{

    private int speed;
    private Digestion digestion;
    private final int stamina;
    private final int legs;
    private int libido;
    private int reproductionCosts;
    private int strength;
    private int minSwimmingEnergyLevel;
    private int minMoveEnergyLevel;
    private int weight;
    private int hunger;
    private Direction direction;
    //private int energy;

    /**
     * Constructor
     *
     * @param x
     * @param y
     * @param digestion
     * @param stamina
     * @param legs
     */
    public Animal(int stamina, int x, int y, Digestion digestion, int legs) {
        super( x, y);
        //this.speed = speed;
        this.digestion = digestion;
        this.stamina = stamina;
        this.legs = legs;
        energy = stamina;
        libido = getLibido();
        //reproductionCosts = stamina / 100 * 80;
        strength = getStrength();
        minSwimmingEnergyLevel = getMinSwimmingEnergyLevel();
        minMoveEnergyLevel = getMinMoveEnergyLevel();
        weight = getWeight();
        hunger = getHunger();
        direction = randomDirection();
        color = getColor();
    }

  
            
            
    public void setDigestion(Digestion digestion) {
        this.digestion = digestion;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    
    @Override
    public void setEnergy(int energy)
    {
        if(energy > getStamina())
        {
            this.energy = getStamina();
        }
        else
        {
        this.energy = energy;
        }
    }
    
    @Override
    public int getEnergy(){
        return energy;
    }  
       
    @Override
    public Color getColor(){
        if(digestion == Digestion.Both)
        {
            color = Color.orange;
        }
        if(digestion == Digestion.Meat)
        {
            color = Color.red;
        }
        if(digestion == Digestion.Nothing)
        {
            color = Color.pink;
        }
        if(digestion == Digestion.Plant)
        {
            color = Color.yellow;
        }
        return color;
    }
    
    /**
     * gets the digestion
     *
     * @return
     */
    public Digestion getDigestion() {
        return digestion;
    }

    /**
     * gets the stamina
     *
     * @return
     */
    @Override
    public int getStamina() {
        return stamina;
    }

    /**
     * gets the libido
     *
     * @return
     */
    public int getLibido() {
        libido = getStamina() / 100 * 95;
        return libido;
       
    }

    /**
     * gets the minimum swimming energy level
     *
     * @return
     */
    @Override
    public int getMinSwimmingEnergyLevel() {
        minSwimmingEnergyLevel = getStamina() / 100 * 50; //stamina swim is 50% van stamina
        return minSwimmingEnergyLevel;
    }

    /**
     * gets minimum move energy level
     * @return 
     */
    public int getMinMoveEnergyLevel() {
        int minMoveEnergyLevel = getStamina() / 100 * 10;
        return minMoveEnergyLevel;
    }

    /**
     * gets the hunger
     * @return 
     */
    @Override
    public int getHunger() {
        int hunger = stamina / 100 * 60;
        return hunger;
    }

    /**
     * gets the speed
     * @return 
     */
    public int getSpeed() {
        speed = getWeight() / 50;

        if (speed < 1) {
            speed = 1;
        }

        return speed;
    }

    /**
     * gets the strength
     * @return 
     */
    public int getStrength() {
        strength = getEnergy();
        return strength;
    }

    /**
     * gets the count legs
     * @return 
     */
    @Override
    public int getLegs() {
        return legs;
    }

    /**
     * gets the weight
     * @return 
     */
    @Override
    public int getWeight() {
        if (weight < 0) {
            weight = 0;
        }

         return weight = legs * 10 + (getEnergy() - getStrength());
    }

    /**
     * gets the reproduction costs
     * @return 
     */
    public int getReproductionCosts() {
        return reproductionCosts;
    }

    /**
     * gets the direction
     * @return 
     */
    @Override
    public Direction getDirection() {
        return direction;
    }

    /**
     * set the direction
     * @param direction 
     */
    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    /**
     * returns a random direction
     * @return 
     */
    public Direction randomDirection() {
        int rand = new Random().nextInt(Direction.values().length);
        return Direction.values()[rand];
    }
    
   
}
