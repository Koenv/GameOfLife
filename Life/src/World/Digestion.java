/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package World;

/**
 * enum of food
 * @author Rene Janssen
 */
public enum Digestion {
    Plant,
    Meat,
    Both,
    Nothing;
}
