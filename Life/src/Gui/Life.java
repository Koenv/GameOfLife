/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gui;

import Persistence.Persistence;
import Simulatie.Simulatie;
import World.Organism;
import World.Gridsection;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import World.World;
import World.Animal;
import static World.Digestion.Plant;
import World.Plant;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;

/**
 *
 * @author Koen
 */
public class Life extends Application implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent event) {

    }

    FXMLDocumentController controller = new FXMLDocumentController();
    private GraphicsContext gc;
    private Stage stage;
    private BorderPane root;
    private Canvas canvas;
    private final Slider speedSlider = new Slider(0.05, 10.0, 10.0);

    public Slider getSpeedSlider() {
        return speedSlider;
    }
    private World world = new World(1000, 1000);
    Simulatie simulatie = new Simulatie();

    /**
     * set the scene and canvas
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        stage.setTitle("Life");

        canvas = new Canvas(960, 930);

        root = setCanvas(canvas, root);//set canvas on borderpane

        //Set the scene
        Scene scene = new Scene(root, canvas.getWidth(), canvas.getHeight() + 25);
        stage.setScene(scene);
        stage.show();
        primaryStage.setOnCloseRequest(e -> System.exit(0));

    }

    /**
     * Starts the inital program and draw's the world, water, obstacles and
     * plants
     *
     * @param e
     */
    public void startGrid(ActionEvent e) {
        world.getGridsections().clear();
        world.getOrganisms().clear();
        int totaalAantal = 100;
        int obstacleA = totaalAantal / 100 * 10;
        int carnivoorA = totaalAantal / 100 * 40;
        int plantA = totaalAantal / 100 * 30;
        int ominvoor = totaalAantal / 100 * 10;
        int herbivoor = totaalAantal / 100 * 5;
        int nonivoorA = totaalAantal / 100 * 5;
        System.out.println("obstacl ==  " + obstacleA);
        try {
            //Draw world's
            world.addWater();
            world.addLand();
            world.addObstacle(obstacleA);
            world.addPlants(plantA);
            world.addAnimals(herbivoor, carnivoorA, nonivoorA, ominvoor);

            //Draw water
            gc = canvas.getGraphicsContext2D();
//            gc.setFill(convertToPaint(Color.blue));
//            gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

//            //Draw obstacles, worlds and water
//            for (Gridsection g : world.getGridsections()) {
//                gc.setFill(convertToPaint(g.getColor()));
//                gc.fillRect(g.getX(), g.getY(), g.getSize(), g.getSize());
//                gc.setStroke(convertToPaint(g.defaultColor()));
//                gc.strokeRect(g.getX(), g.getY(), g.getSize(), g.getSize());
//            }
//
//            //Draw plants/animals
//            for (Organism o : world.getOrganisms()) {
//                gc.setFill(convertToPaint(o.getColor()));
//                gc.fillRect(o.getX(), o.getY(), 10, 10);
//            }
        } catch (Exception f) {
            System.out.println("fout = " + f);
        }

        new AnimationTimer() {
            private long lastUpdate = 0;

            @Override
            public void handle(long currentNanoTime) {
                if ((((double) currentNanoTime - (double) lastUpdate) / 100000000) >= (speedSlider.getValue())) {
                    updateGrid(e);
                    lastUpdate = currentNanoTime;
                }
            }
        }.start();

        //        new Timer().schedule(
        //                new TimerTask() {
        //
        //            @Override
        //            public void run() {
        //                updateGrid(e);
        //            }
        //        }, 0, 200);
    }
    private final double updateTime = 0.016667;

    /**
     * update de grid
     *
     * @param e
     */
    public void updateGrid(ActionEvent e) {
        //world.getGridsections().clear();
        //world.getOrganisms().clear();
        // while(aantStappen <= 2)
        //{
        simulatie.step(world);
        //step++;
        //aantStappen++;
        //life.updateGrid();
        // }
        for (Gridsection g : world.getGridsections()) {
            gc.setFill(convertToPaint(g.getColor()));
            gc.fillRect(g.getX(), g.getY(), g.getSize(), g.getSize());
            gc.setStroke(convertToPaint(g.defaultColor()));
            gc.strokeRect(g.getX(), g.getY(), g.getSize(), g.getSize());
        }

        //Draw plants/animals
        for (Organism o : world.getOrganisms()) {
            gc.setFill(convertToPaint(o.getColor()));
            gc.fillRect(o.getX(), o.getY(), 10, 10);
        }
//        System.out.println("Grootte organisme lijst" + world.getOrganisms().size());
    }

    /**
     * Stop the program
     *
     * @param e
     */
    private void stopProgram(ActionEvent e) {
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        world.getOrganisms().clear();
        world.getGridsections().clear();
    }

    /**
     * Exit the program
     *
     * @param e
     */
    private void exitProgram(ActionEvent e) {
        Platform.exit();
        System.exit(0);
    }

    private void saveProgram(ActionEvent e) {
//       Persistence p = new Persistence();
//       p.saveFile();
        world.saveFile();
    }

    private void loadProgram(ActionEvent e) {
        world.loadFile();
    }
    /**
     * Sets the menubar and all of its menu items
     *
     * @param canvas
     * @param root
     * @return
     */
    public BorderPane setCanvas(Canvas canvas, BorderPane root) {
        //Run menu 1
        javafx.scene.control.Menu menuRun = new javafx.scene.control.Menu("Run");

        //Menu items menuRun
        MenuItem start = new MenuItem("Start");
        start.setOnAction(e -> startGrid(e));
        menuRun.getItems().add(start);
        start.addEventHandler(ActionEvent.ACTION, event -> start.setDisable(true));

        MenuItem stop = new MenuItem("Stop");
        stop.setOnAction(e -> stopProgram(e));
        menuRun.getItems().add(stop);
        stop.addEventHandler(ActionEvent.ACTION, event -> start.setDisable(false));

        MenuItem pauze = new MenuItem("Pauze");
        pauze.setOnAction(e -> System.out.println("Simulatie gepauzeerd"));
        menuRun.getItems().add(pauze);
        pauze.addEventHandler(ActionEvent.ACTION, event -> pauze.setText("Hervatten"));

        menuRun.getItems().add(new SeparatorMenuItem());

        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(e -> exitProgram(e));
        menuRun.getItems().add(exit);

        Persistence ps = new Persistence();
        //Save menu
        javafx.scene.control.Menu menuSave = new javafx.scene.control.Menu("Save");
        //menu items menu save
        MenuItem saveSimulation = new MenuItem("Save simulation");
        saveSimulation.setOnAction(e -> saveProgram(e));
        menuSave.getItems().add(saveSimulation);
        //menu items menu load
        MenuItem loadSimulation = new MenuItem("Load simulation");
        loadSimulation.setOnAction(e -> loadProgram(e));
        menuSave.getItems().add(loadSimulation);

        //Add a slider
        javafx.scene.control.Menu menuSpeed = new javafx.scene.control.Menu("Set speed");
//        slider = new Slider(0.05, 10, 5);
        CustomMenuItem cmi = new CustomMenuItem(speedSlider);
        cmi.setHideOnClick(false);
        menuSpeed.getItems().add(cmi);

        //Main menubar
        MenuBar menubar = new MenuBar();
        menubar.getMenus().addAll(menuRun, menuSave, menuSpeed);

        //Set node
        root = new BorderPane();
        root.setTop(menubar);
        root.setBottom(canvas);

        return root;
    }

    /**
     * converts java.awt.color to javafx.scene.paint.color
     *
     * @param color
     * @return
     */
    private javafx.scene.paint.Color convertToPaint(java.awt.Color color) {
        double red = color.getRed();
        double green = color.getGreen();
        double blue = color.getBlue();

        javafx.scene.paint.Color returnColor = javafx.scene.paint.Color.color(red / 255.0, green / 255.0, blue / 255.0);
        return returnColor;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
