/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulatie;

import World.World;

/**
 *
 * @author Koen
 */
public interface ISimulationGui
{

  /**
   * starts the simulation
     * @param world
   */
  public void start(World world);

  /**
   * stops the simulation
   */
  public void stop();

  /**
   * sets the speed of the simulation (steps).
   *
   * @param speed
   */
  public void setSpeed(double speed);

  /**
   * performs a simulation step.
     * @param world
   */
  public void step(World world);
}
