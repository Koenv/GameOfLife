/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Simulatie;

import Gui.Life;
import World.Animal;
import World.Organism;
import World.World;
import java.util.Iterator;
import java.util.Timer;

/**
 *
 * @author Koen
 */
public class Simulatie implements ISimulationGui {

    private double speed;
    //ISimulationWorld world;
    Timer timer;
    Life life;

    @Override
    public void start(World world) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.    
    }

    @Override
    public void stop() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSpeed(double speed) {
        if (speed < 1) {
            return;
        }
        {
            this.speed = speed;
        }
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     *
     */
    @Override
    public void step(World world) {
        int aantDood = 0;
        int babys = 0;
        for (Iterator<Organism> iterator = world.getOrganisms().iterator(); iterator.hasNext();) {
            Organism org = iterator.next();
            if(org.isDeath()){
                iterator.remove();
                aantDood++;
           }
        }
        
    
        for (Organism o : world.getOrganisms()) {
            if (o instanceof Animal) {
                int moveEnergy = o.getEnergy() - 5;
                o.setEnergy(moveEnergy);
                world.move(o);
//          System.out.println("Beest " + o.getX() + " " +  o.getY() + " " + o.getDirection());
                //System.out.println(o.getY());
            }
        }
        
        for (Organism o : world.getIntercourseOrganisms())
        {
            world.intercourse(o);
        }
        babys = world.getIntercourseOrganisms().size() / 2;
        world.clearIntercourseList();
        
        System.out.println("Doden: " + aantDood + " babys: " + babys + " totaal organisme= " + world.getOrganisms().size());
        
    }
}
