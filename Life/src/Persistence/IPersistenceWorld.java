/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistence;

/**
 *
 * @author Koen
 */
public interface IPersistenceWorld {
    
    /**
     * saves the current condition of the simulation including position of plants, animals and obstacles,
     * number of animals, sorts of animals and the number of plants.
     */
    public void save();
    
    /**
     * loads a previously saved state of a simulation.
     */
    public void load();
}
